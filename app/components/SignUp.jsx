var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var Signup = React.createClass({

    signupToAccount: function (e) {
        e.preventDefault();
        this.props.signupStart(this.refs.email.value, this.refs.password.value);
    },
    render: function () {
        return (
            <div className="container">
                <h3 className="text-center">Signup</h3>
                <form onSubmit={this.loginToAccount}>
                    <div className="form-group">
                        <input required ref="email" type="text" placeholder="your email"/>
                    </div>
                    <div className="form-group">
                        <input required ref="password" type='password' placeholder="password"/>
                    </div>
                    <div className="form-group">
                        <input required ref="comfirmpassword" type="password" placeholder="Confirm password"/>
                    </div>
                    <button onClick={this.signupToAccount}>Login</button>
                </form>
            </div>
        );
    }
});

function mapStateToProps(state) {

    return {};
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        signupStart: actions.signupStart,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Signup);