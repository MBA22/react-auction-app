var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var Navbar = require('Navbar');


var actions = require('./../actions/index');
var Main = React.createClass({

    render: function () {
        return (
            <div>
                <Navbar></Navbar>
                <button className="btn btn-danger" onClick={this.props.logout}>Return to Login</button>

            </div>
        );
    }
});

function mapStateToProps(state) {

    return {
        loginorSignin: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        logout: actions.logoutStart,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Main);