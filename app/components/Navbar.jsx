var React=require('react');
var {Link}=require('react-router');



var Navbar = React.createClass({

    render: function () {
        return (
            <nav>
                <Link to="auction">Auction</Link>
                <Link to="bid">Bid</Link>
            </nav>
        );
    },
});

module.exports = Navbar;