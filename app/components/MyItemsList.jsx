var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var MyItemsList = React.createClass({

    componentDidMount: function () {
        this.props.getMyItems();
    },
    render: function () {
        var myItems = this.props.loginsignupReducer.myItems;
        if (myItems != null) {
            return (
                <div className="row">
                    <h3>My Items</h3>
                    <div className=" well col-xs-12 col-sm-6">
                        {
                            Object.keys(myItems).map(function (key) {
                                if (myItems[key].owner === this.props.loginsignupReducer.email)
                                    return (
                                        <ul className="list-group">
                                            <li className="list-group-item">Name:{myItems[key].name}</li>
                                            <li className="list-group-item">Current Bid:{myItems[key].startingBid}</li>
                                        </ul>
                                    );
                            }.bind(this))
                        }
                    </div>
                </div>
            );
        }
        else
            return (<p>No Items yet</p>);
    }
});

function mapStateToProps(state) {

    return {
        loginsignupReducer: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getMyItems: actions.getItems,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(MyItemsList);