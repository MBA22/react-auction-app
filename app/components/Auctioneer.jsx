var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');
var MyItemsList = require('MyItemsList');

var item = {};
var Auctioneer = React.createClass({

    createAuction: function (e) {
        e.preventDefault();
        var currentDate=new Date();
        item.name = this.refs.itemName.value;
        item.condition = this.refs.condition.value;
        item.startingBid = this.refs.startingBid.value;
        item.owner = this.props.loginsignupReducer.email;
        console.log('expiry',this.refs.expiry.value);
        item.expireDate = String(currentDate.setDate(currentDate.getDate() + parseInt(this.refs.expiry.value,10)));
        this.props.addItem(item);
    },
    render: function () {
        return (
            <div className="container">
                <h3 className="text-center">Auctioneer Component</h3>
                <form onSubmit={this.createAuction}>
                    <div className="form-group">
                    <label>Item Name:</label><input className="form-control" ref="itemName" type="text" placeholder="Item Name"/>
                    </div>
                    <div className="form-group">
                    <label>Condition:</label><select className="form-control" ref="condition" defaultValue='-1'>
                    <option value="-1">select..</option>
                    <option value="poor">Poor</option>
                    <option value="good">Good</option>
                    <option value="excellent">Excellent</option>
                </select>
                    </div>
                    <div className="form-group">
                    <label>Expires in:</label><select className="form-control" ref="expiry" defaultValue='1'>
                    <option value="1">1 day</option>
                    <option value="2">2 days</option>
                    <option value="3">3 days</option>
                </select>
                    </div>
                    <div className="form-group">
                    <label>Starting Bid:</label><input className="form-control" ref="startingBid" type="text" placeholder="Starting Bid"/>
                    </div>
                    <button onClick={this.createAuction}>Create Auction</button>
                </form>

                <MyItemsList></MyItemsList>
            </div>
        );
    }
});

function mapStateToProps(state) {

    return {
        loginsignupReducer: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        addItem: actions.addItem,
        getMyItems: actions.getItems,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Auctioneer);