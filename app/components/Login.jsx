var React = require("react");
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var SignUp = require('SignUp');

var Login = React.createClass({

    changeToSignUp: function (signup) {
        this.props.loginOrSignup(signup);
    },

    loginToAccount: function (e) {
        e.preventDefault();
        this.props.loginStart(this.refs.email.value, this.refs.password.value);
    },
    render: function () {
        if (this.props.loginorSignin.isSigning == 'signup') {
            return (
                <SignUp></SignUp>
            );
        }
        else
            return (
                <div className="container">
                    <h4 className="text-center">Login</h4>
                    <div className="row">
                        <div className="col-xs-4 col-xs-offset-4">
                            <form onSubmit={this.loginToAccount}>
                                <div className="form-group">
                                    <input className="form-control" required ref="email" type="text"
                                           placeholder="your email"/>
                                </div>
                                <div className="form-group">
                                    <input className="form-control" required ref="password" type="password"
                                           placeholder="password"/>
                                </div>
                                <button className="btn btn-primary" onClick={this.loginToAccount}>Login</button>
                            </form>
                            <br/>or<br/>
                            <button className="btn btn-primary" onClick={this.changeToSignUp.bind(this, 'signup')}>
                                SignUp
                            </button>
                        </div>
                    </div>
                </div>
            );
    },
});

function mapStateToProps(state) {

    return {
        loginorSignin: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        loginOrSignup: actions.loginOrSignUp,
        loginStart: actions.loginStart,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Login);