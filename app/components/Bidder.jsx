var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');

var bidValue = '';
var Bidder = React.createClass({
    componentDidMount: function () {
        this.props.setDate();
        this.props.getAllItems();

    },

    componentDidUpdate: function () {
        var myItems = this.props.loginsignupReducer.myItems;
        if (myItems) {
            Object.keys(myItems).map(function (key) {
                if (myItems[key].expireDate != 'expired' && myItems[key].expireDate != undefined) {
                    if (parseInt(myItems[key].expireDate, 10) <= parseInt(this.props.loginsignupReducer.time, 10)) {
                        this.adjustDate(key);
                    }
                }
            })
        }
    },
    formatDate: function (ms) {

        var seconds = ms / 1000;
        var hours = parseInt(seconds / 3600);
        seconds = seconds % 3600;
        var minutes = parseInt(seconds / 60);
        seconds = seconds % 60;
        return ( hours + ":" + minutes + ":" + Math.round(seconds));
    },

    setBid: function (key, event) {
        var bidValue = event.target.previousSibling.value;
        if (bidValue != '') {
            this.props.setBid(key, bidValue);
        }
    },
    setBidValue: function (e) {
        bidValue = e.target.value;
        console.log(e.target.value);
    },
    adjustDate: function (key) {
        this.props.adjustDate(key);
    },
    render: function () {
        var myItems = this.props.loginsignupReducer.myItems;
        if (myItems != undefined) {
            return (
                <div>
                    <h3>Items</h3>

                    {
                        Object.keys(myItems).map(function (key) {
                            if (myItems[key].expireDate != 'expired' && myItems[key].expireDate != undefined) {
                                console.log("EXPIRE DATE", myItems[key].expireDate);
                                var expireDate = new Date(parseInt(myItems[key].expireDate, 10));
                                var currentDate = new Date(parseInt(this.props.loginsignupReducer.time, 10));
                                var difference = expireDate - currentDate;
                                return (
                                    <div className=" well col-xs-12 col-sm-6 col-md-4">
                                    <ul className="list-group" key={key}>
                                        <li className="list-group-item">Name:{myItems[key].name} </li>
                                        <li className="list-group-item">Current Bid:{myItems[key].startingBid}</li>
                                        <input className="form-control" onChange={this.setBidValue} type="number"/>
                                            <button className="btn btn-info" onClick={this.setBid.bind(this, key)}>SetBid</button>
                                        <li className="list-group-item">Current Bidder:{myItems[key].currentBidder}</li>
                                        <li className="list-group-item">Time
                                            Remaining:{this.formatDate(difference)}</li>
                                    </ul>
                                    </div>
                                );
                            }
                            else return (
                                <div className=" well col-xs-12 col-sm-6 col-md-4">
                                <ul className="list-group">
                                    <li className="list-group-item">Name:{myItems[key].name} </li>
                                    <li className="list-group-item">Current Bid:{myItems[key].startingBid}</li>
                                    <input className="form-control" ref="bid" type="number"/>
                                        <button className="btn btn-info" onClick={this.setBid.bind(this, key)}>SetBid</button>
                                    <li className="list-group-item">No bids yet</li>
                                </ul>
                                </div>
                            );
                        }.bind(this))
                    }

                </div>
            );
        }
        else
            return (<p>No Items yet</p>);
    }
});

function mapStateToProps(state) {

    return {
        loginsignupReducer: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getAllItems: actions.getItems,
        setBid: actions.setBid,
        setDate: actions.setDate,
        adjustDate: actions.adjustDate,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Bidder);