var {myFirebase, refFirebase}=require('app/firebase/');
var store = require('./../store/configureStore').storeConfig();
var totalItems = {};

export var setUidOnLogin = function (uid, email) {
    console.log("In Set uid action", uid);
    return {
        type: "LOGIN",
        uid: uid,
        email: email,
    };
};

export var logout = function () {
    console.log("In logout function");

    return {
        type: "LOGOUT",
    };
};
export var loginStart = function (email, password) {
    return function (dispatch, getState) {

        myFirebase.auth().signInWithEmailAndPassword(email, password).then(function (result) {

            dispatch(setUidOnLogin(result.uid));
        }, function (error) {
            console.log("Authentication failed", error);
        });
    }

};
export var signupStart = function (email, password) {
    return function (dispatch, getState) {

        myFirebase.auth().createUserWithEmailAndPassword(email, password).then(function (result) {

            console.log("Authentication worked", result);
            dispatch(setUidOnLogin(result.uid));

            if (email === 'admin@admin.com' && password === 'admin123') {
                dispatch(roleCarrier('admin'));
            }
            else
                dispatch(roleCarrier('user'));

            dispatch(roleCarrier(role));
        }, function (error) {
            console.log("Authentication failed", error);
        });
    }

};
export var logoutStart = function () {
    return function (dispatch, getState) {
        return myFirebase.auth().signOut().then(function () {

            dispatch(logout());
        });
    }
};
export var loginOrSignUp = function (isSinging) {
    return {
        type: "ISLOGINGORSIGNUP",
        isSinging: isSinging,
    };
};


export var startFetching = function () {
    return {
        type: "STARTING_FETCHING",
    };
};
export var doneFetching = function () {
    return {
        type: "DONE_FETCHING",
    };
};


/////////////////////////////////////Items Adding and Fetching////////////////////////////////
export var addedItem = function (item) {
    return {
        type: "ADD_ITEM",
        item: item,
    };
};


export var addItem = function (item) {
    return function (dispatch, getState) {
        var items = refFirebase.ref("items");
        items.push(item);
        dispatch(addedItem(item));

    };
};

export var doneFetchingItems = function (myItems) {
    return {
        type: "DONE_FETCHING_MYITEMS",
        myItems: myItems,
    };
};
export var getItems = function () {
    return function (dispatch, getState) {
        dispatch(startFetching());

        var state = getState();
        var allItemsRef = refFirebase.ref("items");
        allItemsRef.on('value', function (snapshot) {
            totalItems = snapshot.val();
            dispatch(doneFetchingItems(totalItems));
        })

    }
};
export var bidSeted = function (newBid) {
    return {
        type: "BID_SET",
        bid: newBid,
    }
};
export var setBid = function (itemKey, newBid) {
    return function (dispatch, getState) {
        dispatch(startFetching());
        var startingBid = {};
        var state = getState();
        var email = state.loginsignupReducer.email;
        var itemRef = refFirebase.ref(`items/${itemKey}`);
        itemRef.on('value', function (snapshot) {
            startingBid = snapshot.val();
            dispatch(bidSeted(newBid));
        });
        if (Number(startingBid.startingBid) < newBid) {
            var startBid = refFirebase.ref(`items/${itemKey}`)
            startBid.update({'startingBid': newBid, 'currentBidder': email});
        }
        var bids = itemRef.child("bids");
        bids.push(newBid);
    }
};


export var adjustDate = function (key) {
    return (dispatch, getState)
    {
        var itemRef = refFirebase.ref(`items/${key}`);
        var newOwner = '';
        itemRef.on('value', function (snapshot) {
            var item = snapshot.val();
            newOwner = item.currentBidder;
        })
        itemRef.update({
            'expireDate': 'expired',
            'currentBidder': '',
            'owner': newOwner,
        });
    }
    ;
};

export var setDate = function () {
    return function (dispatch, setState) {

setInterval(function () {
            dispatch(settingDate(String(new Date().getTime())));
        }, 1000)
    }
};

export var settingDate = function (time) {
    return {
        type: 'NEW_TIME',
        time: time,
    };
};

