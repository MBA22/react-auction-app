export var loginsignupReducer = function (state = {}, action) {

    switch (action.type) {

        case "ISLOGINGORSIGNUP":
            var newState = Object.assign({}, state, {isSigning: action.isSinging});
            return newState;

        case 'LOGIN':
            var newState = Object.assign({}, state, {uid: action.uid, email: action.email});
            return newState;

        case "LOGINCHECK":

            var newState = Object.assign({}, state, {role: action.role});
            return newState;

        case 'LOGOUT':
            return {};

        case'STARTING_FETCHING':
            var newState = Object.assign({}, state, {fetching: true});
            return newState;

        case 'ADD_ITEM':
            var newState = Object.assign({}, state, {itemAdded: action.item});
            return newState;

        case'BID_SET':
            var newState = Object.assign({}, state, {currentBid: action.bid});
            return newState;

        case 'DONE_FETCHING_MYITEMS':
            var newState = Object.assign({}, state, {fetching: false, myItems: action.myItems});
            return newState;

        case'NEW_TIME':
            var newState = Object.assign({}, state, {time: action.time});
            return newState;
        default:
            return state;
    }
};
