module.exports = {
    entry: './app/app.jsx',
    output: {
        path: __dirname,
        filename: './public/bundle.js'
    },
    resolve: {
        root: __dirname,
        alias: {
            Main: 'app/components/Main.jsx',
            Navbar: 'app/components/Navbar.jsx',
            Login: 'app/components/Login.jsx',
            SignUp: 'app/components/SignUp.jsx',
            Auctioneer: 'app/components/Auctioneer.jsx',
            Bidder: 'app/components/Bidder.jsx',
            MyItemsList: 'app/components/MyItemsList.jsx',
        },
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                },
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/
            }
        ]
    }
};
